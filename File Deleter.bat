cls

@ECHO OFF

title File Deleter v1.1 (By Areeb)

REM [WELCOME] Welcome screen

:WELCOME
color 02
cls
echo                    -------------------------------------                                   
echo                            [ File Deleter 1.1 ]
echo                           [ www.areeb-beigh.tk ]          
echo                    -------------------------------------  
echo  Welcome to File Deleter v1.1! 
echo.
echo  1. Take tour #1
echo  2. Take tour #2
echo  3. Take tour #3
echo  4. Delete a File/Folder
echo  5. Credits
echo  6. Changelog
echo  7. Exit
echo.
set/p "opt=>"
if %opt%==1 ( goto TUT1 ) 
if %opt%==2 ( goto TUT2 ) 
if %opt%==3 ( goto TUT3 ) 
if %opt%==4 ( goto START1 ) 
if %opt%==5 ( goto CREDITS ) 
if %opt%==6 ( goto CLOG ) 
if %opt%==7 ( goto EXIT ) 
if %opt% GTR 7 ( goto WINVLD )

REM [WELCOME] Invalid input

:WINVLD
color 0C
cls 
echo [ERROR]
echo.
echo The input you entered is invalid. Please try again.
echo.
pause
goto WELCOME

REM Tutorial

:TUT1
color 03
cls
echo                    -------------------------------------                                   
echo                            [ File Deleter 1.1 ]
echo                           [ www.areeb-beigh.tk ]          
echo                    -------------------------------------  
echo                                 TUTORIAL 1
echo  -----
echo  How to use:
echo  It's Simple. The program will ask you the name of the file to be deleted. 
echo  Enter the name or path to the file to be deleted WITH the
echo  extension and press enter. Then you'll get a confirmation message
echo  reply with Y for YES and N for NO.
echo  -----
pause
echo  -----
echo  If you specify the name of a directory or folder all the files within
echo  it will be deleted but the directory and the subdirectories will stay.
echo  To delete the content in the subdirectories specify the path to them.
echo  (ex: files\office.txt)
echo  -----
pause
echo  -----
echo  If you specify the name of a file or a path to a file then only that file
echo  will be deleted.
echo  -----
pause
echo  -----
echo  If you have a file or folder with a "space" in between the name characters 
echo  then put the name you enter in "inverted comas".
echo  (ex: "New file.txt")
echo  -----
pause
echo  -----
echo  Feel free to contact me trough my website for any help / suggestions. 
echo  Thank You :)!
echo  -----
pause
goto WELCOME

REM Tutorial #2

:TUT2
color 03
cls
echo                    -------------------------------------                                   
echo                            [ File Deleter 1.1 ]
echo                           [ www.areeb-beigh.tk ]          
echo                    -------------------------------------  
echo                                TUTORIAL 2
echo This is a tutorial to show you how to delete MULTIPLE files with 
echo prompts for each file seperately in a specified directory.
pause
echo -----
echo 1. Place this file in the target directory i.e The directory in 
echo    which the files to be deleted exist 
echo                        OR
echo    Specify the full path to the target directory.
echo -----
pause
echo -----
echo 2. Type "." [dot] for the file name and press enter.
echo    (ex: C:\Users\Areeb\Desktop\.)
echo -----
pause
echo -----
echo 3. Now you'll receive a prompt to delete every single file present
echo    in the specified directory. Enter Y for the ones you want to delete
echo    and N for the ones you want to skip. Happy deleting :)!
echo -----
pause
echo NOTE: This method will allow you to delete ANY file in the directory. 
echo       Even GHOST FILES which normally cannot be detected or deleted.
echo -----
pause
goto WELCOME

REM Tutorial #3

:TUT3
color 03
cls
echo                    -------------------------------------                                   
echo                            [ File Deleter 1.1 ]
echo                           [ www.areeb-beigh.tk ]          
echo                    -------------------------------------  
echo                                TUTORIAL 3
echo This tutorial is to show you how to delete all the files and folders 
echo in a directory.
pause
echo -----
echo 1. Go to "Delete a File / Fodler" then select "Folder / Directory"
echo -----
echo 2. Enter the name of the directory.
pause
echo -----
echo 3. Now you'll be asked to select a delete method. Select the desired 
echo    method and press enter. 
pause
echo -----
echo 4. After selecting the method you'll be asked to confirm the deletion
echo    reply with Y to confirm. 
pause
echo -----
echo Success! The file and folders in the directory will be deleted! 
echo (Depending on which opt you selected the directory will also get deleted)
pause
goto WELCOME


REM [Delete process] Choose file type

:START1
color 0C
cls
echo                    -------------------------------------                                   
echo                            [ File Deleter 1.1 ]
echo                           [ www.areeb-beigh.tk ]          
echo                    -------------------------------------  
echo                                   DELETE
echo Enter the type of file to be deleted.
echo.
echo 1. File (.txt, .exe, .bat, .lnk, .ppt etc)
echo 2. Folder / Directory
echo 3. Go back
echo.
set/p "opt2=>"
if %opt2%==1 ( goto DFile ) 
if %opt2%==2 ( goto DFolder )
if %opt2%==3 ( goto WELCOME )
if %opt2% GTR 3 ( goto DINVLD )

REM [Delete process] Invalid input

:DINVLD
color 0C
cls
echo [ERROR]
echo.
echo The value you entered is invalid. Please try again.
echo.
pause
goto START1

REM [Delete process] Delete File

:DFile
color 0A
cls
echo Enter the name of the file to be deleted.
set/p "filename=>"
if EXIST %filename% ( goto PROCESS1 ) 
if NOT EXIST %filename% ( goto ERROR )
if %filename%=="File Deleter.bat" ( goto ERROR2 )

REM [Delete process] Delete Folder / Directory

:DFolder 
color 0A
cls
echo Enter the name of the folder / directory to be deleted.
set/p "filename=>"
if EXIST %filename% ( goto PROCESS2 ) 
if NOT EXIST %filename% ( goto ERROR )

REM [Delete process] Set file attributes

:PROCESS1
ATTRIB -H %filename%
goto DELETE1

:PROCESS2
ATTRIB -H %filename%
goto PRE-FDELETE

REM [Delete process] Delete file

:DELETE1
cls
del /P %filename%
echo -----
echo Delete successful.
echo -----
pause
goto START1

REM [Delete process] Delete method

:PRE-FDELETE
cls 
color 08
echo Enter the delete method.
echo.
echo 1. Delete all the files and folders in the directory "%filename%" 
echo    but not the directory itself.
echo 2. Delete all the files and folders in the directory "%filename%" 
echo    and the directory itself.
echo 3. Go back.
echo.
set/p "opt3=>"
if %opt3%==1 ( goto DELETE3 )
if %opt3%==2 ( goto DELETE2 )
if %opt3%==3 ( goto START1 )
if %opt3% GTR 3 ( goto PINVLD )

REM [Delete process] Invalid input

:PINVLD
color 0C
cls
echo [ERROR]
echo.
echo The value you entered is invalid. Please try again.
echo.
pause
goto PRE-FDELETE

REM [Delete process] Full delete

:DELETE2
cls
echo Directory %filename% including it's contents will be deleted.
pause
color 0C 
RD /s %filename%
pause
color 0A
cls
echo -----
echo Delete successful.
echo -----
pause
goto START1

REM [Delete process] Partial delete

:DELETE3
cls
echo All files and folders in the directory %filename% will be deleted.
echo but not the directory itself.
pause
color 0C 
RD /s %filename%
MD %filename%
pause
color 0A
cls
echo -----
echo Delete successful.
echo -----
pause
goto START1

REM Error 404

:ERROR
color 0C
cls
echo                    -------------------------------------                                   
echo                            [ File Deleter 1.1 ]
echo                           [ www.areeb-beigh.tk ]          
echo                    -------------------------------------  
echo [ERROR 404] The file "%filename%" could not be found. 
echo -----
echo       You can try:
echo    1. Re-check the filename and try again.
echo    2. Specify the full path to the file. (ex: C:\Users\Admin\Example.exe)
echo    3. Make sure you specify the extesion of the file too.
echo    4. If the file you want to delete is a SHORTCUT then put the extension 
echo       ".lnk" with it's name.
echo -----
pause
goto START1

REM File cannot be deleted

:ERROR2
color 0C
cls
echo [ERROR] 
echo.
echo The specified file cannot be deleted.
echo.
pause
goto START1

REM Credits

:CREDITS
color 0C
cls 
echo                    -------------------------------------                                   
echo                            [ File Deleter 1.1 ]
echo                           [ www.areeb-beigh.tk ]          
echo                    -------------------------------------  
echo                                  CREDITS
pause
color 0C
cls
echo                    -------------------------------------                                   
echo                            [ File Deleter 1.1 ]
echo                           [ www.areeb-beigh.tk ]          
echo                    -------------------------------------  
echo                           Improvemtnt Suggestions 
echo                               Hossein Newman
pause
cls 
color 09
echo                    -------------------------------------                                   
echo                            [ File Deleter 1.1 ]
echo                           [ www.areeb-beigh.tk ]          
echo                    -------------------------------------  
echo                            Software developer
if EXIST "Typograph.txt" ( type Typograph.txt ) else ( echo                               Areeb Beigh )
START www.areeb-beigh.tk
pause
goto WELCOME

REM Changelog

:CLOG
color 08
cls 
echo                    -------------------------------------                                   
echo                            [ File Deleter 1.1 ]
echo                           [ www.areeb-beigh.tk ]          
echo                    -------------------------------------  
echo                            Changelog 1.0 - 1.1
echo.
echo 1. Added featured option to delete directories.
echo 2. Added invalid input response.
echo 3. Fixed issue with hidden files.
echo 4. Various script optimizations.
echo.
pause
goto WELCOME

REM [EXIT] Start screen - Exit

:EXIT
color 0E
cls
echo Are you sure you want to exit? (Y/N)
set/p opt=">"
if /I %opt%==Y ( EXIT ) else ( if /I %opt%==N ( goto WELCOME ) else ( goto EINVLD ) )

REM [EXIT] Invalid input

:EINVLD
color 0C
cls
echo [ERROR]
echo.
echo The input you entered is invalid. Please reply with Y/N. 
echo.
pause
goto EXIT 
