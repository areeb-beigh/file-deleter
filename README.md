# NOTE
**My projects are active only on GitHub, these are only the initial releases on BitBucket. GitHub: https://github.com/areeb-beigh**

# File Deleter v1.1

A simple &amp; small program coded in batch (.bat) with multiple deleting features. This program comprises all the deletion features on windows into a single software &amp; is a convenient way of mass deleting, deleting ghost files (those irritating 0B files that are not detected by the system!), hidden files & much more.

**How to use**

##How to delete a file

1. Open File Deleter.bat
2. If the file to be deleted is present in the same directory / folder as File Deleter.bat is present then simply enter the name of the file with the extension.
   For example: test.txt
   If the file to be deleted in not present in the same folder / directory as File Deleter is then you need to specify the full path of the file.
   For example: C:\Users\Areeb\Desktop\test.txt
   NOTE: If the file to be deleted has a name with a space in it then put the file name in "inverted comas". 
   For example: "records 2015.txt" OR "C:\Users\Areeb\Desktop\records 2015.txt"				
3. Press ENTER. 
4. You will receive a confirmation message before the file is deleted. Reply with Y for YES or N for NO.

***

##How to delete contents in a folder / directory	 

1. Open File Deleter.bat
2. Type in the path to the folder / directory.
3. Press enter.
4. Now you'll receive a confirmation message for the deletion of every file present in the folder. Reply with Y for YES and N for NO.
	 NOTE: Optionally in the v1.1 you may use the "Delete Folder / Directory" option instead.
	 
***

##How to delete a GHOST file

Ghost files can be pretty annoying as they can't be deleted easily because the system does not detect them. This File Deleter can also help you delete ghost files from your computer.
    
1. Open File Deleter.bat
2. Type in the path to the folder / directory in which the ghost file is present and in the last field type in . [dot].	 
   For example: "C:\Users\Areeb\Desktop\."
   NOTE: . [dot] means a wildcard entry to delete every file in the target directory so you'll have to reject / accept delete confirmations one by one.
3. Now browse through the files simply by entering N for the files you don't wont to delete and when you find the file to be deleted reply with Y.

***
	 
##Deleting a hidden file

1. Open File Deleter.bat
2. Specify the path to the hidden file.
   NOTE: Even if the file is hidden File Deleter can detect the file.
3. Reply with Y when you get a confirmation message.
	
****

* **Developer**: Areeb Beigh
* **Website**: www.areeb-beigh.tk
* **Version**: 1.1